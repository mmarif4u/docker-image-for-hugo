FROM ubuntu:18.04

MAINTAINER M M Arif

# Update core system and install rsync, openssh-client
RUN apt-get update && apt-get install -y wget openssh-client rsync

# Download and install hugo 
ENV HUGO_VERSION 0.52 
ENV HUGO_BINARY hugo_${HUGO_VERSION}_Linux-64bit.deb

RUN wget https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY} && \
	dpkg -i ${HUGO_BINARY} && \
	rm ${HUGO_BINARY}