# Docker image for Hugo

**This docker image is based on Ubuntu 18.04 and has few extra tools installed for GitLab CI like openssh-client and rsync.**

## Current Docker image:  
https://hub.docker.com/r/mmarif4u/ubuntu-hugo-gitlab

# Note (future implementaton only)

I will be migrating from docker hub to gitlab registery. This repo will be updated soon.

#### Build and push:
`docker build -t mmarif4u/ubuntu-apache:latest .`  

`docker push mmarif4u/ubuntu-hugo-gitlab:latest`

